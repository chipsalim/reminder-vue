import { app } from './app';

app.$mount('#app');

// Rebuilding Middleware config.
if (module.hot) {
    module.hot.accept();
}

export { app };
