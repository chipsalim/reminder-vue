const webpack = require('webpack');
const clientConfig = require('./webpack.client.config');

module.exports = function setupDevServer (app, onUpdate) {
    // Hot Reloading Middleware Configuration
    clientConfig.entry.app = [
        'webpack-hot-middleware/client',
        clientConfig.entry.app
    ]

    clientConfig.plugins.push(
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin()
    )

    // Re-building Middleware Configuration
    const clientCompiler = webpack(clientConfig)

    app.use(
        require('webpack-dev-middleware')(clientCompiler, {
            stats: {
                colors: true
            }
        })
    );

    app.use(require('webpack-hot-middleware')(clientCompiler))
};
