# Reminder Hub
A reminder application that notifies people when a reminder is added or completed.

# Features
- Push notification when a reminder is added/completed.
- Common categories